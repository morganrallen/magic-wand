Controller portion of [Sprite Wand] project.

The Sprite can be found at [Sprite]

![preview](board_preview.png)

[Sprite]: https://gitlab.com/morganrallen/sprite
[Sprite Wand]: https://hackaday.io/project/165714-sprite-wand
